Everett Jewelry is Shreveport Bossier City’s destination for diamonds and engagement rings. With over 2,456 engagement rings, each customized to fit your style, you are going to find your perfect one-of-a kind ring here. Free lifetime warranty on engagement rings. Custom design and jewelry repairs.

Address: 1819 E 70th St, Shreveport, LA 71105, USA

Phone: 318-795-0860

Website: http://www.everettjewelry.com
